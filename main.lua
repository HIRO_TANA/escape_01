-- 初期設定
display.setStatusBar(display.HiddenStatusBar)

_W         = display.contentWidth
_H         = display.contentHeight

CENTER_X   = display.contentCenterX
CENTER_Y   = display.contentCenterY

__alpha    = 0

ImgDir     = 'Asset/Image/'
JsonDir    = 'Asset/Json/'
ViewDir    = 'View.'
ModelDir   = 'Model.'
ModDir     = 'Modu.'
ContDir    = 'Cont.'

composer   = require "composer"
json       = require "json"
ads        = require (ModDir .. "ads")
require (ModDir .."library")
require (ModDir .. "print") 

composer.gotoScene( ContDir .. 'start')