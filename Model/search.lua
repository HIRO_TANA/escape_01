local EventDispatcher = system.newEventDispatcher()
local Search = setmetatable( {}, { __index = EventDispatcher } )

local data = {}

local rawData = json.decode(readText(JsonDir .. 'story.json', system.ResourceDirectory))

function Search.getData( chapterId, id )
	return data[tonumber(chapterId)][tonumber(id)]
end

for _, story in pairs (rawData) do
	if not data[story['chapter']] then
		data[story['chapter']] = {}
	end

	if not data[story['chapter']][story['sectionId']] then
		data[story['chapter']][story['sectionId']] = {}
	end

	local storyList = data[story['chapter']][story['sectionId']]

	if not storyList['id'] then
		storyList['id'] = {}
	end

	if not storyList['chapter'] then
		storyList['chapter'] = {}
	end

	if not storyList['sectionId'] then
		storyList['sectionId'] = {}
	end

	if not storyList['gimmickObjectName'] then
		storyList['gimmickObjectName'] = {}
	end

	if not storyList['gimmickObjectId'] then
		storyList['gimmickObjectId'] = {}
	end

	if not storyList['text'] then
		storyList['text'] = {}
	end

	if not storyList['item'] then
		storyList['item'] = {}
	end

	if not storyList['image'] then
		storyList['image'] = {}
	end

	if not storyList['location'] then
		storyList['location'] = {}
	end

	if not storyList['progress'] then
		storyList['progress'] = {}
	end

	table.insert( storyList['id'], story['id'] )
	table.insert( storyList['chapter'], story['chapter'] )
	table.insert( storyList['sectionId'], story['sectionId'] )
	table.insert( storyList['gimmickObjectName'], story['gimmickObjectName'] )
	table.insert( storyList['gimmickObjectId'], story['gimmickObjectId'] )
	table.insert( storyList['text'], story['text'] )
	table.insert( storyList['item'], story['item'])
	table.insert( storyList['image'], story['image'] )
	table.insert( storyList['location'], story['location'] )
	table.insert( storyList['progress'], story['progress'])
end

return Search
