local EventDispatcher = system.newEventDispatcher()
local MainShaft = setmetatable( {}, { __index = EventDispatcher } )

function MainShaft:moveFloor(floorNum)
	self.floorNum = tonumber( floorNum ) or 1
	self:dispatchEvent{
		name = 'move_floor',
		floorNum = self.floorNum
	}
end

function MainShaft:goNextChapter(chapterId)
	self.chapterId = tonumber(chapterId) or 1
end

function MainShaft:goNextSection(sectionId)
	self.sectionId = tonumber(sectionId) or 1
end

function MainShaft:getChapterId()
	return self.chapterId
end

function MainShaft:getSectionId()
	return self.sectionId
end

function MainShaft.new()
	local mainConf = {
		floorNum = 1,
		chapterId = 1,
		sectionId = 1,
	}
	setmetatable(mainConf, { __index = MainShaft })
	return mainConf
end

return MainShaft
