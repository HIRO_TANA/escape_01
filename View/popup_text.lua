local itemPopup = require(ViewDir .. "item_popup")
local AnimView = require(ViewDir .. "animation")

local PopupText = {}

function PopupText.new(group)
	local popupText = display.newGroup()
	group:insert(popupText)

	local function finishStory(selif, progress, item)
		local function onComplete()
			if progress then
				local function goNextSection()
					popupText:dispatchEvent{
						name = 'go_next_section',
					}
					popupText:destroy()
				end

				AnimView.showBlackFilter(popupText.animLayer, nil, nil, goNextSection)
			else
				popupText:destroy()
			end
		end

		if popupText.page > #selif then
			if (item ~= "") then
				popupText.itemPopup:show(item)
				popupText.itemPopup:addEventListener('get_item', onComplete)
			else
				onComplete()
			end
			return true
		end
		return false
	end

	function popupText:setContent(selif, progress, item)
		if finishStory(selif, progress, item) then
			return
		end
		print( '全ページ数', #selif )
		print( '現在のページ数', self.page )

		local function showStoryContent()
			if self.text then
				display.remove( self.text )
				self.text = nil
			end

			local content = selif[self.page]['serif']
			self.text = display.newText{
				parent = self.contentLayer,
				text = content,
				x = CENTER_X,
				y = CENTER_Y+300,
				width = _W-120,
				font = NormalFont,
				fontSize = 24,
				align = 'left'
			}

			self.page = self.page + 1
		end

		showStoryContent(selif)
	end

	function popupText:tap(event)
		local selif = event.target.text
		local progress = event.target.progress
		local item = event.target.item
		self:setContent(selif, progress, item)
		return true
	end

	function popupText:createUi(group, text, progress, item)
		local selifPopup = display.newImage(group, ImgDir .."popup_text/popup.png", CENTER_X, _H-190)

		local selifTapArea = display.newRect(group, CENTER_X, CENTER_Y, _W, _H)
		selifTapArea.alpha = 0
		selifTapArea.text = text
		selifTapArea.progress = progress
		selifTapArea.item = item
		selifTapArea.isHitTestable = true
		selifTapArea:addEventListener("tap", function(event)
			self:tap(event)
		end)

		self.page = 1
		self:setContent(text, progress, item)
	end

	function popupText:show(text, progress, item)
		self.group = display.newGroup()
		self:insert(self.group)

		self.bottomLayer = display.newGroup()
		self.group:insert(self.bottomLayer)

		self.uiLayer = display.newGroup()
		self.group:insert(self.uiLayer)

		self.contentLayer = display.newGroup()
		self.group:insert(self.contentLayer)

		self.animLayer = display.newGroup()
		self.group:insert(self.animLayer)

		local tapFilter = display.newRect(self.bottomLayer, CENTER_X, CENTER_Y, 640, 1136)
		tapFilter.alpha = __alpha
		tapFilter.isHitTestable = true
		tapFilter:setFillColor( 1, 0, 0 )
		tapFilter:addEventListener("tap", function(event) return true end)

		self.itemPopup = itemPopup.new(self.animLayer)

		self:createUi(self.uiLayer, text, progress, item)
	end

	function popupText:destroy()
		display.remove( self.group )
		self.group = nil
	end

	return popupText
end

return PopupText
