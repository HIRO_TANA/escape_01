local AnimView = require(ViewDir .. "animation")

local RoomDetail = {}

local RoomDetailOption = {
	{floorName = "dollRoom",    x = {CENTER_X, CENTER_X*3}, fileName = "b1_doll_room_bg",    floorNum = 1},
	{floorName = "storageRoom", x = {CENTER_X, CENTER_X*3}, fileName = "b1_storage_room_bg", floorNum = 1},
	{floorName = "playroom",    x = {CENTER_X, CENTER_X*3}, fileName = "b1_play_room_bg",    floorNum = 1},
	{floorName = "livingRoom",  x = {CENTER_X, CENTER_X*3}, fileName = "1f_living_room_bg",  floorNum = 2},
	{floorName = "toiletRoom",  x = {CENTER_X, CENTER_X*3}, fileName = "1f_shower_room_bg",  floorNum = 2},
	{floorName = "mensRoom",    x = {CENTER_X, CENTER_X*3}, fileName = "2f_boy_room_bg",     floorNum = 3},
	{floorName = "girlsRoom",   x = {CENTER_X, CENTER_X*3}, fileName = "2f_girl_room_bg",    floorNum = 3},
}

function RoomDetail.new(group)
	local roomDetail = display.newGroup()
	group:insert(roomDetail)

	function roomDetail:showRoom(floorName)
		local floorNameList = {}

		local function onComplete()
			for key, value in pairs (RoomDetailOption) do
				table.insert(floorNameList, value.floorName)
			end

			self.roomId = table.indexOf( floorNameList, floorName)
			self.roomList[self.roomId][1].isVisible = true

			for i = 1, #self.itemList do
				if (self.itemList[i].location == floorName) then
					self.itemList[i].isVisible = true
					break
				end
			end

			self.uiLayer.isVisible = true
		end

		AnimView.showBlackFilter(self.animLayer, nil, nil, onComplete)
	end

	function roomDetail:tapButton(event)
		if event.target.tag == "leave_room" then
			self:dispatchEvent{
				name = 'leave_room',
				id = self.roomId,
				floorNum = RoomDetailOption[self.roomId].floorNum
			}
		elseif event.target.tag == "tap_gimmick" then
			self:dispatchEvent{
				name = 'tap_gimmick',
				text = event.target.text,
				item = event.target.item,
				progress = event.target.progress
			}
		end

		return true
	end

	function roomDetail:createUi(group)
		local backBtn = display.newImage(group, ImgDir.."room_detail/leave.png", CENTER_X-200, _H-160)
		local tapAreaBackBtn = display.newRect(group, CENTER_X-200, _H-160, 150, 100)
		tapAreaBackBtn.alpha = __alpha
		tapAreaBackBtn.tag = "leave_room"
		tapAreaBackBtn.isHitTestable = true
		tapAreaBackBtn:setFillColor( 1, 0, 0 )
		tapAreaBackBtn:addEventListener("tap", function( event ) self:tapButton(event) end)
	end

	function roomDetail:createRoom(group)
		self.roomList = {}
		for i = 1, 7 do
			local room = RoomDetailOption[i]
			local roomFileName = RoomDetailOption[i].fileName

			local roomDeatilList = {}
			for j = 1, 2 do
				roomDeatilList[j] = display.newImage(group, ImgDir.."room_detail_bg/"..roomFileName..j..".jpg", room.x[j], CENTER_Y)
				roomDeatilList[j].isVisible = false
				roomDeatilList[j].name = RoomDetailOption[i].floorName
			end
			table.insert(self.roomList, roomDeatilList)
		end
	end

	function roomDetail:createGimmickObject(group, data)
		self.itemList = {}
		for i = 1, #data.id do
			local gimmickobject = {}
			gimmickobject = display.newImage(group, ImgDir.."gimmickobjects/"..data['image'][i], CENTER_X, CENTER_Y+100)
			gimmickobject.gimmickName = data['gimmickObjectName'][i]
			gimmickobject.location = data['location'][i]
			gimmickobject.tapArea = display.newRect(group, gimmickobject.x, gimmickobject.y, 150, 150)
			gimmickobject.tapArea.alpha = __alpha
			gimmickobject.text = data['text'][i]
			gimmickobject.item = data['item'][i]
			gimmickobject.progress = data['progress'][i]
			gimmickobject.isVisible = false
			gimmickobject.tag = "tap_gimmick"

			gimmickobject:addEventListener("tap", function(event) self:tapButton(event) end)

			table.insert(self.itemList, gimmickobject)
		end
	end

	function roomDetail:show(data)
		self.bottomLayer = display.newGroup()
		self:insert(self.bottomLayer)

		self.uiLayer = display.newGroup()
		self:insert(self.uiLayer)

		self.itemLayer = display.newGroup()
		self:insert(self.itemLayer)

		self.animLayer = display.newGroup()
		self:insert(self.animLayer)

		local tapFilter = display.newRect(self.bottomLayer, CENTER_X, CENTER_Y, 640, 1136)
		tapFilter.alpha = __alpha
		tapFilter.isHitTestable = true
		tapFilter:setFillColor( 1, 0, 0 )
		tapFilter:addEventListener("tap", function(event) return true end)

		self:createRoom(self.bottomLayer)
		self:createUi(self.uiLayer)
		self:createGimmickObject(self.itemLayer, data)

		self.uiLayer.isVisible = false
	end

	function roomDetail:hide(roomId)
		self.roomList[roomId][1].isVisible = false

		for i = 1, #self.itemList do
			if (self.itemList[i].location == self.roomList[roomId][1].name) then
				self.itemList[i].isVisible = false
				break
			end
		end

		self.uiLayer.isVisible = false
	end

	function roomDetail:allHide()
		for i = 1, 7 do
			self.roomList[i][1].isVisible = false

			for j = 1, #self.itemList do
				self.itemList[j].isVisible = false
			end
		end
		self.uiLayer.isVisible = false
	end

	return roomDetail
end

return RoomDetail
