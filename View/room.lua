local AnimView = require(ViewDir .. "animation")

local Room = {}

local FooterOption = {
	{x = CENTER_X, y = _H-124, floorNum = 1, areaX = CENTER_X - 200, areaY = _H-140},
	{x = CENTER_X, y = _H-125, floorNum = 2, areaX = CENTER_X      , areaY = _H-140},
	{x = CENTER_X, y = _H-135, floorNum = 3, areaX = CENTER_X + 200, areaY = _H-140}
}

local PageOption = {
	{
		x = CENTER_X, y = CENTER_Y, floorNum = 1, floorCount = 3,
		floor_X = {CENTER_X-80, CENTER_X-110, CENTER_X+100},
		floor_Y = {CENTER_Y-130, CENTER_Y+100, CENTER_Y+100},
		floor_W = {250, 170, 220},
		floor_H = {150, 200, 200},
		floorName = {"dollRoom", "storageRoom", "playroom"}
	},
	{
		x = CENTER_X, y = CENTER_Y, floorNum = 2, floorCount = 2,
		floor_X = {CENTER_X-90, CENTER_X+120},
		floor_Y = {CENTER_Y, CENTER_Y+100},
		floor_W = {220, 160},
		floor_H = {400, 170},
		floorName = {"livingRoom", "toiletRoom"}
	},
	{
		x = CENTER_X, y = CENTER_Y, floorNum = 3, floorCount = 2,
		floor_X = {CENTER_X-120, CENTER_X+90},
		floor_Y = {CENTER_Y, CENTER_Y+100},
		floor_W = {160, 220},
		floor_H = {400, 200},
		floorName = {"mensRoom", "girlsRoom"}
	}
}

function Room.new(group)
	local room = display.newGroup()
	group:insert(room)

	function room:setIsHitTestable(floorNum)
		local count = self.tapPageList[floorNum].floorCount

		for i = 1, count do
			self.tapPageList[floorNum][i].isHitTestable = true
		end
	end

	function room:change( floorNum )
		local floorNum = floorNum or 1
		local count

		local function onComplete()
			for i = 1, 3 do
				count = self.tapPageList[i].floorCount

				self.floorFooter[i].isVisible = self.mainShaft.floorNum == i

				self.floorPage[i].isVisible = self.mainShaft.floorNum == i
				self.tapPageList[i].isHitTestable = self.mainShaft.floorNum == i

				for j = 1, count do
					self.tapPageList[i][j].isHitTestable  = false
				end
			end
			self:setIsHitTestable(floorNum)
		end

		AnimView.showBlackFilter(self.animLayer, nil, nil, onComplete)
	end

	function room:showFloor(group)

		function self:tapFloor(event)
			local floorNum = event.target.floorNum
			self.mainShaft:moveFloor(floorNum)

			return true
		end

		self.floorFooter = {}
		self.tapFooterArea = {}
		for i = 1, 3 do
			local floor = FooterOption[i]
			self.floorFooter[i] = display.newImage(group, ImgDir.."room/floor/"..i..".png", floor.x, floor.y)
			self.floorFooter[i].floorNum = floor.floorNum
			self.floorFooter[i].isVisible = self.mainShaft.floorNum == i

			self.tapFooterArea[i] = display.newRect(group, floor.areaX, floor.areaY, 180, 100)
			self.tapFooterArea[i].floorNum = floor.floorNum
			self.tapFooterArea[i].alpha = __alpha
			self.tapFooterArea[i].isHitTestable = true
			self.tapFooterArea[i]:setFillColor( 1, 0, 0 )
			self.tapFooterArea[i]:addEventListener("tap", function( event ) self:tapFloor(event) end)
		end
	end

	function room:showFramePage(group)
		local floorFrame = display.newImage(group, ImgDir.."room/frame.png", CENTER_X, CENTER_Y)

		self.floorPage = {}
		self.tapPageList = {}
		for i = 1, 3 do
			local page = PageOption[i]
			self.floorPage[i] = display.newImage(group, ImgDir.."room/frameb"..i..".png", page.x, page.y)
			self.floorPage[i].floorNum = page.floorNum
			self.floorPage[i].isVisible = self.mainShaft.floorNum == i

			self.tapArea = {}
			for j = 1, page.floorCount do
				self.tapArea[j] = display.newRect(group, page.floor_X[j], page.floor_Y[j], page.floor_W[j], page.floor_H[j])
				self.tapArea[j].floorName = page.floorName[j]
				self.tapArea[j].alpha = __alpha
				self.tapArea[j]:setFillColor(1, 0, 0)
				self.tapArea[j]:addEventListener("tap", function( event )
					self.floorName = event.target.floorName
					self:dispatchEvent{
						name = 'select_room',
						floorName = self.floorName
					}
				end)
			end
			table.insert(self.tapPageList, self.tapArea)
			self.tapPageList[i].isVisible = false
			self.tapPageList[i].isHitTestable = false
			self.tapPageList[i].floorCount = page.floorCount

		end
	end

	function room:show()
		self.bottomLayer = display.newGroup()
		self:insert(self.bottomLayer)

		self.animLayer = display.newGroup()
		self:insert(self.animLayer)

		local tapFilter = display.newRect(self.bottomLayer, CENTER_X, CENTER_Y, 640, 1136)
		tapFilter.alpha = __alpha
		tapFilter.isHitTestable = true
		tapFilter:setFillColor( 1, 0, 0 )
		tapFilter:addEventListener("tap", function(event) return true end)

		local roomBg = display.newImage(self.bottomLayer, ImgDir .."room/bg.png", CENTER_X, CENTER_Y)
		self:showFloor(self.bottomLayer)
		self:showFramePage(self.bottomLayer)

		-- 起動時に表示するFloorのタップ判定をtrueにする.
		self:setIsHitTestable(self.mainShaft.floorNum)

		local footer = display.newImage(self.bottomLayer, ImgDir .."room/footer.png", CENTER_X, _H-50)
		local header = display.newImage(self.bottomLayer, ImgDir .."room/header.png", CENTER_X, CENTER_Y-400)

		self.headerText = display.newText{
			parent = self.bottomLayer,
			text = "現在："..self.mainShaft.chapterId.."章-"..self.mainShaft.sectionId.."節",
			x = CENTER_X,
			y = CENTER_Y-330,
			width = _W-120,
			font = NormalFont,
			fontSize = 36,
			align = 'left'
		}
		header:scale(1, -1)
	end

	return room
end

return Room
