local ItemPopup = {}

function ItemPopup.new(group)
	local itemPopup = display.newGroup()
	group:insert(itemPopup)

	function itemPopup:create(item)
		local itemPopupUi = display.newImage(self.uiLayer, ImgDir .. "get/item.png", CENTER_X, CENTER_Y)

		local item = display.newImage(self.contentLayer, ImgDir .. "item/"..item, CENTER_X-180, CENTER_Y-200)
		item.xScale = 0.4
		item.yScale = 0.4

		local closeButton = display.newRect(self.uiLayer, CENTER_X, CENTER_Y-50, 200, 100)
		closeButton.alpha = __alpha
		closeButton.isHitTestable = true
		closeButton:addEventListener("tap", function(event)
			self:dispatchEvent{
				name = 'get_item',
			}
			self:destroy()
			return true
		end)
	end

	function itemPopup:show(item)
		self.group = display.newGroup()
		self:insert(self.group)

		self.bottomLayer = display.newGroup()
		self.group:insert(self.bottomLayer)

		self.uiLayer = display.newGroup()
		self.group:insert(self.uiLayer)

		self.contentLayer = display.newGroup()
		self.group:insert(self.contentLayer)

		local tapFilter = display.newRect(self.bottomLayer, CENTER_X, CENTER_Y, 640, 1136)
		tapFilter.alpha = __alpha
		tapFilter.isHitTestable = true
		tapFilter:setFillColor( 1, 0, 0 )
		tapFilter:addEventListener("tap", function(event) return true end)

		self:create(item)
	end

	function itemPopup:destroy()
		display.remove( self.group )
		self.group = nil
	end

	return itemPopup
end

return ItemPopup
