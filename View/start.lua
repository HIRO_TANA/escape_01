local Start = {}

function Start.new(group)
	local start = display.newGroup()
	group:insert(start)

	function start:show()
		self.bottomLayer = display.newGroup()
		self:insert(self.bottomLayer)

		local uiLayer = display.newGroup()
		self.bottomLayer:insert(uiLayer)

		local startButton = display.newRect(uiLayer, CENTER_X, CENTER_Y+200, 200, 100)
		startButton:addEventListener("tap", function ()
			start:dispatchEvent{name = "start_game"}
		end)
	end

	return start
end

return Start
