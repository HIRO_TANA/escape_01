local Anim = {}

function Anim.showBlackFilter(group, time, delay, onComplete)
	Anim.blackFilter = display.newRect(group, CENTER_X, CENTER_Y, 640, 1136)
	Anim.blackFilter.alpha = 0
	Anim.blackFilter.isHitTestable = true
	Anim.blackFilter:setFillColor(0)
	Anim.blackFilter:addEventListener("tap", function(event) return true end)
	
	local options = {
		time = time or 500,
		delay = delay or 200,
		onComplete = onComplete or nil
	}

	transition.to(Anim.blackFilter, {time = options.time, alpha = 1, transition = easing.outSine, onComplete = function()
		if onComplete then
			options.onComplete()
		end
		transition.to(Anim.blackFilter, {time = options.time, delay = options.delay, transition = easing.inSine, alpha = 0, onComplete = function()
			display.remove(Anim.blackFilter)
			Anim.blackFilter = nil
		end})
	end})
end

return Anim
