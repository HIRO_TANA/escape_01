local scene = composer.newScene()

--view
local StartView = require(ViewDir .. "start")

--model
local MainShaft = require( ModelDir .. 'mainShaft' )

--module
local Ads = require( ModDir .. 'ads' )

local views = {}

local function startGame()
    composer.gotoScene(ContDir .. "room")
end

local function initData()
    composer.mainShaft = MainShaft.new()
    startGame()
end

function scene:create( event )
    local sceneGroup = self.view
    views.startView = StartView.new(sceneGroup)
    views.ads = ads.new(sceneGroup)
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        views.startView:show()
        views.startView:addEventListener( "start_game", initData)

        -- 広告表示
        views.ads:show()
    elseif ( phase == "did" ) then
    end
end


-- hide()
function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        views.startView:removeEventListener("start_game", initData)
    elseif ( phase == "did" ) then
    end
end


-- destroy()
function scene:destroy( event )
    local sceneGroup = self.view
    for key, _ in pairs( views ) do
        views[key] = nil
    end
end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene
