local scene = composer.newScene()

--view
local RoomView       = require(ViewDir .. "room")
local RoomDetailView = require(ViewDir .. "room_detail")
local AnimView       = require(ViewDir .. "animation")
local PopupTextView  = require(ViewDir .. "popup_text")

--model
local SearchModel = require( ModelDir .. 'search' )

local views = {}

local function changeMap(event)
    local floorNum = event.floorNum
    views.roomView:change(floorNum)
end

local function transSelectRoom(event)
    local floorName = event.floorName
    views.roomDetailView:showRoom(floorName)
    views.roomDetailView.isVisible = true
end

local function transHome(event)
    local roomId = event.id
    views.roomDetailView:hide(roomId)
    views.roomDetailView.isVisible = false
end

local function goNextSection(event)
    local sectionId = mainShaft:getSectionId()
    mainShaft:goNextSection(sectionId+1)

    if(mainShaft.sectionId > 7)then
        composer.gotoScene( ContDir .. 'start')
        return true
    end

    views.roomView.headerText.text = "現在："..mainShaft.chapterId.."章-"..mainShaft.sectionId.."節"

    views.roomDetailView:allHide()
    views.roomDetailView.isVisible = false

    local searchData = SearchModel.getData(mainShaft.chapterId, mainShaft.sectionId)
    views.roomDetailView:show(searchData)
end

local function showStory(event)
    local selifData = {}
    for k, v in pairs( event ) do
        if k == 'text' then
            for i = 1, #v do
                local comment = v[i]:gsub( '「', '' )
                comment = comment:gsub( '」', '' )
                selifData[#selifData+1] = {
                    serif = comment
                }
            end
        end
    end
    views.popupTextView:show(selifData, event.progress, event.item)
end

function scene:create( event )
    local sceneGroup = self.view
    views.roomView = RoomView.new(sceneGroup)
    views.roomDetailView = RoomDetailView.new(sceneGroup)
    views.popupTextView = PopupTextView.new(sceneGroup)
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        mainShaft = composer.mainShaft
        views.roomView.mainShaft = mainShaft
        views.roomDetailView.mainShaft = mainShaft
        views.popupTextView.mainShaft = mainShaft

        views.roomView:show()

        local searchData = SearchModel.getData(mainShaft.chapterId, mainShaft.sectionId)
        views.roomDetailView:show(searchData)

        views.roomDetailView.isVisible = false


        views.roomView:addEventListener('select_room', transSelectRoom)

        local function onComplete(event)
            AnimView.showBlackFilter(sceneGroup, nil, nil, function()
                transHome(event)
            end)
        end
        views.roomDetailView:addEventListener('leave_room', onComplete)
        views.roomDetailView:addEventListener('tap_gimmick', showStory)
        views.popupTextView:addEventListener('go_next_section', goNextSection)

        mainShaft:addEventListener( 'move_floor', changeMap )
    elseif ( phase == "did" ) then

    end
end


-- hide()
function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        views.roomView:removeEventListener('select_room', transSelectRoom)
        mainShaft:removeEventListener('move_floor', changeMap)
        views.roomDetailView:removeEventListener('tap_gimmick', showStory)
        views.popupTextView:removeEventListener('go_next_section', goNextSection)
    elseif ( phase == "did" ) then
    end
end


-- destroy()
function scene:destroy( event )
    local sceneGroup = self.view
    for key, _ in pairs( views ) do
        views[key] = nil
    end
end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene
