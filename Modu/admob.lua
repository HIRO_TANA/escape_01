local admob = require( "plugin.admob" )

local Admob = {}

local function show()
	admob.show( "banner" , { y = _H })
end

local function adListener( event )
	if ( event.phase == "init" ) then
		admob.load( "banner", { adUnitId = "ca-app-pub-3940256099942544/6300978111" } )
		admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-3940256099942544/5224354917" } )
		admob.load( "interstitial", { adUnitId = "ca-app-pub-3940256099942544/1033173712" } )
	elseif ( event.phase == "loaded" ) then
		show()
	end
end

function Admob.init()
	local options = {
		appId     = "ca-app-pub-3325116139668819~9825204508",
		testMode  = false
	}
	admob.init( adListener, options )
end

return Admob
