local admob = require(ModDir .. "admob")
local Ads = {}

function Ads.new(group)
	local ads = display.newGroup()
	group:insert(ads)

	local views = {}
	function ads:show()
		views.bannerView = admob.init()
	end

	return ads
end

return Ads
