local lfs = require "lfs"

function readText(textName, dir)
	local directory = dir or system.DocumentsDirectory

	local path = system.pathForFile(textName, directory)
	local file, errorString = io.open(path, "r")

	if file then
		local contents = file:read("*a")

		io.close(file)
		return contents
	else
		print( "File error: " .. errorString )
		return nil
	end

	file = nil
end
